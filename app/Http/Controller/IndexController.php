<?php
/**
 * Created by PhpStorm.
 * User: chensf1
 * Date: 2018/7/28
 * Time: 13:53
 */

namespace App\Http\Controller;

use App\Http\MyController;
use Dingo\Api\Routing\Helpers;

class IndexController extends MyController
{
    use Helpers;
    public function index()
    {
        $apiDomain = env('API_DOMAIN');

        $help = [
            'user' =>  $apiDomain . '/user',
        ];
        return $this->response->array([
            'data' => $help
        ]);
    }
}
<?php

namespace App\Http\Controller\Auth;

use App\Http\MyController;
use Dingo\Api\Routing\Helpers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Hash;

class LoginController extends MyController
{
    use AuthenticatesUsers;
    use Helpers;

    public function login(Request $request)
    {
        $credentials = $request->only('password', 'username');
        try
        {
            //\JWTFactory::setTTL(1);
            if (!$token = auth()->attempt($credentials))
            {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e)
        {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json([
            'data' => [
                'username' => JWTAuth::user()->username,
                'id' => JWTAuth::user()->id,
                'token' => $token
            ]
        ]);
    }

    public function sendLoginResponse(Request $request, $token)
    {
        $this->clearLoginAttempts($request);

        return $this->authenticated($token);
    }

    public function authenticated($token)
    {
        return $this->response->array([
            'data' => [
                'token' => $token
            ]
        ]);
    }

    public function sendFailedLoginResponse()
    {
        throw new UnauthorizedHttpException("Bad Credentials");
    }

    public function logout()
    {
        $this->guard()->logout();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: chensf1
 * Date: 2018/7/28
 * Time: 13:53
 */

namespace App\Http\Controller;

use App\Http\MyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends MyController
{
    public function upload(Request $request)
    {

        $path = $request->file('file')->store('public/upload');
        $path = 'http://'.$_SERVER["SERVER_NAME"].Storage::url($path);
        return $this->response->array([
            'data' => $path
        ]);
    }
}
<?php

namespace App\Http\Controller;

use App\Http\MyController;
use App\Models\User;
use App\Transformer\BasicModelTransformer;
use App\Transformer\UserModelTransformer;
use Dingo\Api\Http\Request;
use Dingo\Api\Routing\Helpers;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use DB;
use Carbon\Carbon;

class UsersController extends MyController
{
    use Helpers;
    public function list(Request $request) {
		$productList = User::paginate($request->limit);
		return $this->response->paginator ( $productList, BasicModelTransformer::class );
	}
    public function index(Request $request)
    {
        $user = User::paginate($request->per_page);

        return $this->response->paginator($user, UserModelTransformer::class);
    }
    public function info(Request $request)
    {
        return response()->json([
            'data' => [
                "name" => "admin",
                "avatar" => "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
                "roles"=> [
                    "admin",
                    "operator"
                ]
            ]
        ]);
    }

    public function getUserTrend() {
        $this_day = [Carbon::today(), Carbon::tomorrow()];
        $this_week = [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()];

        $userList = DB::table('user')
                 ->whereBetween('created_at', $this_week)
                 ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as created_at_date'), DB::raw('count(*) as total'))
                 ->groupBy('created_at_date')
                 ->orderByDesc('created_at_date')
                 ->get()
                 ->toArray();
        $userCount = DB::table('user')
                 ->whereBetween('created_at', $this_day)
                 ->count();

        $labels = [];
        $time = array_column($userList, 'created_at_date');
        foreach ($time as $key => $item) {
            $labels[] = date('m-d',strtotime($item));
        }
        return $this->response->array([
            'data' => [
                'todayCount'=> $userCount,
                'labelData'=>$labels,
                'lineData'=>array_column($userList, 'total')
            ]
        ]);
    }

    public function get($uid)
    {
        $user = User::find($uid);

        if (empty($user))
        {
            $this->response->errorNotFound();
        }

        return $this->response->item($user, UserModelTransformer::class);
    }

    public function update(Request $request, $uid)
    {
        $user = User::find($uid);

        if (empty($user))
        {
            $this->response->errorNotFound();
        }

        $data = $request->json('data');
        $user->update($data);

        return $this->response->item($user, UserModelTransformer::class);
    }

    public function delete($uid)
    {
        $user = User::find($uid);
        if (is_null($user))
        {
            $this->response->errorNotFound();
        }

        $user->organizations()->detach();
        $user->delete();

        return $this->response->item($user, UserModelTransformer::class);
    }

    public function getUserOrganizations($uid)
    {
        $user = User::find($uid);
        if (is_null($user))
        {
            $this->response->errorNotFound();
        }

        return $this->response->collection($user->organizations, BasicModelTransformer::class);
    }

    public function useOrganization(Request $request, $uid)
    {
        $org = $request->json('data');

        $user = User::find($uid);
        $user->active_org_id = $org['id'];

        $org = $user->organizations()->find($org['id']);
        $user->role = $org ? $org->roles->role : '';

        $user->update();

        return $this->response->item($user, UserModelTransformer::class);
    }

    public function getAuthenticatedUser()
    {
        try
        {
            if (!$user = JWTAuth::parseToken()->authenticate())
            {
                $this->response->errorNotFound('user_not_found');
            }
        } catch (TokenExpiredException $e)
        {
            $this->response->errorUnauthorized('token_expired');
        } catch (TokenInvalidException $e)
        {
            $this->response->errorUnauthorized('token_invalid');
        } catch (JWTException $e)
        {
            $this->response->errorUnauthorized('token_absent');
        }

        return $this->response->item($user, UserModelTransformer::class);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: wangjiansheng
 * Date: 2020/10/19
 * Time: 11:08 AM
 */

namespace App\Http\Controller;


use App\Http\MyController;
use App\Services\Project\ProjectService;
use App\Transformer\BasicModelTransformer;
use Dingo\Api\Http\Request;

class ProjectController extends MyController
{
    /**
     * @var \Illuminate\Foundation\Application|mixed
     */
    public $projectService;

    public function __construct(){
        $this->projectService = app(ProjectService::class);
    }
    /**
     * 项目列表查询
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function list(Request $request) {
        $name = $request->query('name');
        $qa = $request->query('qa');
        $pm = $request->query('pm');
        $scale = $request->query('scale');
        $controlMode = $request->query('controlMode');
        $risk = $request->query('risk');
        $perPage = $request->get('per_page', 10);
        $list = $this->projectService->searchProject(
            $name,
            $qa,
            $pm,
            $scale,
            $controlMode,
            $risk,
            $perPage
        );
        return $this->response->paginator($list,  BasicModelTransformer::class);
    }
    /**
     * 获取项目详情
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function info(Request $request) {
        $id = $request->query('id');
        $project = $this->projectService->getProjectById($id);
        if ($project) {
            return $this->response->item($project, BasicModelTransformer::class);
        }
        $this->response->errorNotFound();
    }

    /**
     * 项目规模统计
     * @return mixed
     */
    public function scaleStatistics(){
        $scaleStatistics = $this->projectService->getProjectCountByScale();

        if($scaleStatistics) {
            return $this->response->array(array("data" => $scaleStatistics));
        }
        $this->response->errorNotFound();
    }

    /**
     * 研发模式统计
     * @return mixed
     */
    public function devModeStatistics(){
        $statistics = $this->projectService->getProjectCountByDevMode();
        if($statistics) {
            return $this->response->array(array("data" => $statistics));
        }
        $this->response->errorNotFound();
    }

    /**
     * 研发掌控模式统计
     * @return mixed
     */
    public function controlModeStatistics(){
        $statistics = $this->projectService->getProjectCountByControlMode();
        if($statistics) {
            return $this->response->array(array("data" => $statistics));
        }
        $this->response->errorNotFound();
    }

    /**
     * 项目阶段统计
     * @return mixed
     */
    public function stageStatistics(){
        $statistics = $this->projectService->getProjectCountByStage();
        if($statistics) {
            return $this->response->array(array("data" => $statistics));
        }
        $this->response->errorNotFound();
    }

    /**
     * 项目人员统计
     * @return mixed
     */
    public function qaStatistics(){
        $statistics = $this->projectService->getProjectCountByQa();
        if($statistics) {
            return $this->response->array(array("data" => $statistics));
        }
        $this->response->errorNotFound();
    }

    /**
     * 统计整体项目数量情况：项目总数、人员总数、风险总数等
     * @return mixed
     */
    public function projectTotalStatistics(){
        $statistics = $this->projectService->getProjectTotalCount();
        if($statistics) {
            return $this->response->array(array("data" => $statistics));
        }
        $this->response->errorNotFound();
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: chensf1
 * Date: 2018/7/27
 * Time: 19:04
 */

namespace App\Http;


use Illuminate\Routing\Controller as BaseController;
use Dingo\Api\Routing\Helpers;

class MyController extends BaseController
{
    use Helpers;
}
<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DetailClassifyResource extends Resource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
		$products = $this->products->groupBy ( function ($item, $key) {
			return $item->length . 'x' . $item->width;
		} )->map ( function ($product) {
			return SimpleProductResource::collection ( $product );
		} );
		
		$result = collect ( [ ] );
		foreach ( $products as $size => $list ) {
			$category = array ();
			$category ['size'] = $size;
			$category ['list'] = $list;
			$result->push ( $category );
		}
		return [ 
				'id' => $this->id,
				'name' => $this->name,
				'level' => $this->level,
				'products' => $result 
		];
	}
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\PriceHistory;
use App\Models\Classify;

use Log;

class DetailProductResource extends Resource {
	
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
		// $historyPrice = PriceHistory::where ( 'merchant_product_id', $this->id )->orderByDesc ( 'updated_at' )->limit(15)->get()->toArray();
		$classify = Classify::ancestorsAndSelf($this->product->classify_id)->toArray();
		$icon = $this->getClassifyIcon($classify);
        $classifyName = array_column($classify, 'name');
        $lastIndex = count($classifyName) - 1;
        unset($classifyName[0]);
        if (count($classifyName) >=2 ) {
            unset($classifyName[$lastIndex]);
        }
		return [
				'id' => $this->id,
                'product_id' => $this->product->id,
				'name' => $this->product->name,
				'color' => $this->product->color,
				'weight' => is_numeric($this->product->weight) ? number_format($this->product->weight, '2'): $this->product->weight,
				'length' => $this->product->length,
				'width' => $this->product->width,
				'thickness' => is_numeric($this->product->thickness) ? number_format($this->product->thickness, '2'): $this->product->thickness,
                'long_width' => $this->product->long_width,
				'short_width' => $this->product->short_width,
				'quality' => $this->product->quality,
				'outer_diameter' => $this->product->outer_diameter,
                'description' => $this->product->description,
                'icon' => !empty($icon) ? 'http://'.$_SERVER["SERVER_NAME"].$icon : '',
                'price' => number_format($this->price,'2'),
                'change' => number_format($this->change,'2'),
				// 'price_trend' => array_column($historyPrice, 'price'),
				'price_trend' => [],
                'classify' => implode('-', $classifyName)
		];
	}

	private function getClassifyIcon($classify) {
	    foreach ($classify as $item) {
	        if (!empty($item['icon'])) {
	            return $item['icon'];
            }
        }
        return '';
    }
}

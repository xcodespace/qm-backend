<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SimpleProductCategoryResource extends Resource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
		return [
				'id' => $this->id,
				'name' => $this->name,
				'description' => $this->description,
				'path' => $this->path,
				'status' => $this->status,
				'pic_url' => $this->pic_url,
				'parent_id' => $this->parent_id,
				'category' => SimpleProductCategoryResource::collection($this->children),
		];
	}
}

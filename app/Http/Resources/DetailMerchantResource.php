<?php

namespace App\Http\Resources;

use App\Models\Classify;
use Illuminate\Http\Resources\Json\Resource;
use App\Models\MerchantProduct;
use App\Models\Product;

class DetailMerchantResource extends Resource {
	
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
		$merchantProduct = MerchantProduct::where ( 'merchant_id', $this->id )->get()->toArray();
		$productIds = array_column($merchantProduct, 'product_id');
		$products = Product::find($productIds)->toArray();
		$classifyIds = array_column($this->classify->toArray(), 'classify_id') ;
		$classify = Classify::whereIn('id', $classifyIds)->get(['icon', 'name'])->toArray();
		foreach ($classify as $index=>$item) {
              $classify[$index]['icon'] = !empty($item['icon']) ? 'http://'.$_SERVER['SERVER_NAME'].$item['icon'] : '';
        }
		return [
			'id' => $this->id,
			'name' => $this->name,
			'description' => $this->description,
			'address' => $this->address,
			'phone' => $this->phone,
			'contact' => $this->contact,
			'products' => array_column($products, 'name'),
            'classify' => !empty($classify) ? $classify : []
		];
	}
}

<?php

namespace App\Http\Resources;

use App\Models\Classify;
use Illuminate\Http\Resources\Json\Resource;

class SimpleProductResource extends Resource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
        $classify = Classify::ancestorsAndSelf($this->product->classify_id)->toArray();
        $classifyName = array_column($classify, 'name');
        $icon = $this->getClassifyIcon($classify);

        $lastIndex = count($classifyName) - 1;
        unset($classifyName[0]);
        if (count($classifyName) >=2 ) {
            unset($classifyName[$lastIndex]);
        }
		return [
				'id' => $this->id,
                'product_id' => $this->product->id,
                'name' => $this->product->name,
                'length' => $this->product->length,
                'width' => $this->product->width,
                'weight' => is_numeric($this->product->weight) ? number_format($this->product->weight, '2'): $this->product->weight,
				'thickness' => is_numeric($this->product->thickness) ? number_format($this->product->thickness, '2'): $this->product->thickness,
                'color' => $this->product->color,
                'long_width' => $this->product->long_width,
				'short_width' => $this->product->short_width,
				'quality' => $this->product->quality,
				'outer_diameter' => $this->product->outer_diameter,
                'description' => $this->product->description,
                'classify_id' => $this->product->classift_id,
                'icon' => !empty($icon) ? 'http://'.$_SERVER["SERVER_NAME"].$icon : '',
                'location' => '',
				'price' => number_format($this->price,'2'),
				'change' => number_format($this->change,'2'),
                'classify' => implode('-', $classifyName)
        ];
	}
    private function getClassifyIcon($classify) {
        foreach ($classify as $item) {
            if (!empty($item['icon'])) {
                return $item['icon'];
            }
        }
        return '';
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SimpleMerchantProductResource extends Resource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
		return [
				'merchant_product_id' => $this->id,
				'price' => $this->price,
                'name' => $this->product->name,
                'classify_id' => $this->product->classify_id,
                'length' => $this->product->length,
                'width' => $this->product->width,
				'weight' => is_numeric($this->product->weight) ? number_format($this->product->weight, '2'): $this->product->weight,
				'thickness' => is_numeric($this->product->thickness) ? number_format($this->product->thickness, '2'): $this->product->thickness,
                'description' => $this->product->description,
                'color' => $this->product->color
		];
	}
}

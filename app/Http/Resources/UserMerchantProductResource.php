<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserMerchantProductResource extends Resource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
		return [ 
				'merchant_product_id' => $this->id,
				'price' => number_format($this->price, 2),
				'change' => number_format($this->change, 2),
				'product' => new DetailProductResource ( $this ),
				'order_number' => $this->whenPivotLoaded ( 'user_product', function () {
					return $this->pivot->order_number;
				} ),
				'region' => $this->region && $this->region->getName () ? $this->region->getName () : '--'
		];
	}
}

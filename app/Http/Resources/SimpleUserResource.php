<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SimpleUserResource extends Resource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
		return [ 
				'id' => $this->id,
				'wx_id' => $this->wx_id,
				'real_name' => $this->real_name,
				'username' => $this->username,
				'info' => $this->info,
				'avatar' => $this->avatar
		];
	}
}

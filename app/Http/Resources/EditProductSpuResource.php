<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\ProductAttrValue;
use App\Models\ProductAttr;
use Log;

class EditProductSpuResource extends Resource {
	
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
		$skus = $this->skus;
		return [
				'id' => $this->id,
				'category_id' => $this->category_id,
				'name' => $this->name,
				'description' => $this->description,
				'unit' => $this->unit,
				'banner_url' => $this->banner_url,
				'main_url' => $this->main_url,
				'price' => $this->price,
				'status' => $this->status,
				'skus' => $this->getSkuInfo($skus),
				'attrs' => $this->getAttrs($skus)
		];
	}


	/**
	 * 获取SKU ID
	 *
	 * @param [type] $skus
	 * @return void
	 */
	private function getSkuInfo($skus) {
		$skuInfoArr = [];
		foreach ($skus as $key => $sku) {
			$id = $sku->id;
			$attrValues = explode(',', $sku->attrs);
			$attrValuesModels = ProductAttrValue::findMany($attrValues);
			// attr 与 attr_value的关联
			$attrs = [];
			foreach ($attrValuesModels as $index => $item) {
				$attr = ProductAttr::find($item->attr_id);
				$attrs[] = [
					'k_id' => $item->attr_id,
					'k' => $attr->name,
					'v_id'=>$item->id,
					'v'=>$item->value
				];
			}
			$attrIds = array_unique(\array_column($attrs, 'attr_id'));
			$attrValues = \array_column($attrs, 'attr_value');
			$ids = [];
			foreach($attrs as $key=>$attr) {
				$ids[] = $attr['k_id'].'-'.$attr['v_id'];
			}
			// 组合
			$skuInfoArr[] = [
				'ids'=>implode('_', $ids),
				'price'=>$sku->price,
				'skus'=>$attrs
			];
		}
		return $skuInfoArr;
	}


	/**
	 * 获取spu的所有属性信息
	 *
	 * @param [type] $skus
	 * @return void
	 */
	private function getAttrs($skus) {
		$attrs = [];
		// 属性ID
		$attrIds = $this->getAttrIds($skus);
		// 属性值ID
		$attrValues = $this->getAttrValues($skus);
		Log::info($attrValues);
		// 获取属性值
		foreach($attrIds as $key => $attrId) {
			$attr = ProductAttr::find($attrId);
			$values = ProductAttrValue::where('attr_id', $attrId)->whereIn('id', $attrValues)->get(['id','value'])->toArray();
			$temp['leaf'] = $values;
			$temp['id'] = $attr->id;
			$temp['value'] = $attr->name;
			$attrs[] = $temp;
		}
		return $attrs;
	}

	/**
	 * 获取sku 所有属性值 ID
	 *
	 * @param [type] $skus
	 * @return void
	 */
	private function getAttrValues($skus) {
		// 属性值
		$attrValues = [];
		// 获取属性值
		foreach ($skus as $key => $sku) {
			$attrs = explode(',', $sku->attrs);
			$attrValues = array_merge($attrValues, $attrs);
		}
		return array_unique($attrValues);
	}

	/**
	 * 获取sku 的所有属性 ID
	 *
	 * @param [type] $skus
	 * @return void
	 */
	private function getAttrIds($skus) {
		$attrValues = $this->getAttrValues($skus);
		$attrValueModels = ProductAttrValue::findMany($attrValues);
		// 属性ID
		$attrIds = [];
		foreach ($attrValueModels as $key => $attrValueModel) {
			$attrModel = $attrValueModel->attr;
			$attrIds[] = $attrModel->id;
		}
		return array_unique($attrIds);
	}

}

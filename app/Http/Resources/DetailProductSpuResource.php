<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\ProductAttrValue;
use App\Models\ProductAttr;


class DetailProductSpuResource extends Resource {
	
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
		$category = $this->category;
		$skus = $this->skus;
		return [
				'category_info' => $category,
				'skus' => $this->getSkuInfo($skus),
				'spu_info' => $this->getSpuInfo(),
				'attrs' => $this->getAttrs($skus)
		];
	}

	/**
	 * 获取spu信息
	 *
	 * @return void
	 */
	private function getSpuInfo() {
		// spu 信息
		return [
			'name' => $this->name,
			'description' => $this->description,
			'unit' => $this->unit,
			'banner_url' => $this->banner_url,
			'main_url' => $this->main_url,
			'price' => $this->price,
			'status' => $this->status
		];
	}

	/**
	 * 获取sku attr与id的对应
	 *
	 * @param [type] $skus
	 * @return void
	 */
	private function getSkuIdAttrMap($skus) {
		$skuIdAttrValMap = [];
		// 获取属性值
		foreach ($skus as $key => $sku) {
			$attrs = explode(',', $sku->attrs);
			$skuIdAttrValMap[$sku->id] = $attrs;
		}
		$skuIdAttr = [];
		foreach ($skuIdAttrValMap as $key => $attrs) {
			foreach ($attrs as $index => $attr) {
				$skuIdAttr[$attr][] = $key;
				array_unique($skuIdAttr[$attr]);
			}
		}
		return $skuIdAttr;
	}

	/**
	 * 获取SKU ID
	 *
	 * @param [type] $skus
	 * @return void
	 */
	private function getSkuInfo($skus) {
		$skuInfoArr = [];
		foreach ($skus as $key => $sku) {
			$id = $sku->id;
			$attrValues = explode(',', $sku->attrs);
			$attrValuesModels = ProductAttrValue::findMany($attrValues);
			// attr 与 attr_value的关联
			$attrs = [];
			foreach ($attrValuesModels as $index => $item) {
				$attrs[] = [
					'attr_id'=>$item->attr_id,
					'attr_value'=>$item->value
				];
			}
			$attrIds = array_unique(\array_column($attrs, 'attr_id'));
			$attrValues = \array_column($attrs, 'attr_value');
			// 组合
			$skuInfoArr[] = [
				'skuId'=>$id,
				'price'=>$sku->price,
				'attrs'=>$attrs
			];
		}
		return $skuInfoArr;
	}
	/**
	 * 获取sku 所有属性值
	 *
	 * @param [type] $skus
	 * @return void
	 */
	private function getAttrValues($skus) {
		// 属性值
		$attrValues = [];
		// 获取属性值
		foreach ($skus as $key => $sku) {
			$attrs = explode(',', $sku->attrs);
			$attrValues = array_merge($attrValues, $attrs);
		}
		return array_unique($attrValues);
	}

	/**
	 * 获取sku 的所有属性
	 *
	 * @param [type] $skus
	 * @return void
	 */
	private function getAttrIds($skus) {
		$attrValues = $this->getAttrValues($skus);
		$attrValueModels = ProductAttrValue::findMany($attrValues);
		// 属性ID
		$attrIds = [];
		foreach ($attrValueModels as $key => $attrValueModel) {
			$attrModel = $attrValueModel->attr;
			$attrIds[] = $attrModel->id;
		}
		return array_unique($attrIds);
	}

	/**
	 * 获取spu的所有属性信息
	 *
	 * @param [type] $skus
	 * @return void
	 */
	private function getAttrs($skus) {
		$attrs = [];
		$skuIdAttr = $this->getSkuIdAttrMap($skus);
		$attrIds = $this->getAttrIds($skus);
		$attrValues = $this->getAttrValues($skus);
		// 获取属性值
		foreach($attrIds as $key => $attrId) {
			$attr = ProductAttr::find($attrId);
			$values = ProductAttrValue::where('attr_id', $attrId)->whereIn('id', $attrValues)->get();
			foreach ($values as $index => $value) {
				$value->skus = $skuIdAttr[$value->id];
			}
			$attr->values = $values;
			$attrs[] = $attr;
		}
		return $attrs;
	}
}

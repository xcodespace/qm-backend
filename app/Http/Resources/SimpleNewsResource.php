<?php
/**
 * Created by PhpStorm.
 * User: wangjiansheng
 * Date: 2020/2/20
 * Time: 4:06 PM
 */

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\Resource;

class SimpleNewsResource extends Resource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "type" => $this->type,
            "source" => $this->source,
            "url" => $this->url,
            "title" => $this->title,
            "secondTitle" => $this->second_title,
            "publishDate" => $this->publish_date
        ];
    }
}
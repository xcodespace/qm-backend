<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SimpleShopClassifyResource extends Resource {
	/**
	 * Transform the resource into an array.
	 *
	 * @param \Illuminate\Http\Request $request        	
	 * @return array
	 */
	public function toArray($request) {
		return [
				'id' => $this->id,
				'name' => $this->name,
				'tag' => $this->tag,
				'pcs' => $this->pcs,
				'parent_id' => $this->parent_id,
				'price_trend' => [ ],
				'icon' => !empty($this->icon) ? 'http://'.$_SERVER["SERVER_NAME"].$this->icon : '',
				'region' => null, // TODO 分类暂不分区域
				'price' => number_format($this->price, 2),
				'tax_price' => number_format($this->tax_price, 2),
				'change' => number_format($this->change, 2)
		];
	}
}

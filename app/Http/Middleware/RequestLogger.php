<?php

namespace App\Http\Middleware;

use App\Models\RequestLog;
use Auth;
use Closure;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RequestLogger
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var Response $response */
        $response = $next($request);

        try {
            // Insert Request Log
            $this->saveLog($request, $response);
        } catch (Exception $exception) {
            //
        }

        return $response;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param Response $response
     * @return RequestLog|\Illuminate\Database\Eloquent\Model
     */
    private function saveLog($request, $response)
    {
        $requestLog = RequestLog::create([
            'version' => method_exists($request, 'version') ? $request->version() : '',
            'method' => $request->getMethod(),
            'uri' => $request->route()->uri,
            'full_url' => $request->fullUrl(),
            'queries' => $request->query(),
            'headers' => $request->header(),
            'user_agent' => $request->userAgent(),
            'remote_addr' => $request->ip(),
            'parameters' => $request->route()->parameters,
            'request_content' => $request->getContent(),
            'request_content_length' => $request->hasHeader('Content-Length') ? (int)($request->header('Content-Length')) : 0,
            'user_id' => Auth::guard()->user() ? Auth::guard()->user()->getAuthIdentifier() : null,
            'response_content' => $response->getContent(),
            'response_content_length' => mb_strlen($response->getContent()),
            'status_code' => $response->getStatusCode(),
        ]);

        return $requestLog;
    }
}

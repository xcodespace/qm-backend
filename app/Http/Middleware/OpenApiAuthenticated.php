<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class OpenApiAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!auth()->check() && !$request->is("openapi/auth/login") && !$request->is("openapi/wechat/auth/mplogin")) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $next($request);
    }
}

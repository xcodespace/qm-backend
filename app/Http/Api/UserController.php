<?php

namespace App\Http\Api;

use App\Http\MyController;
use App\Http\Resources\UserMerchantProductResource;
use App\Http\Resources\SimpleProductResource;
use App\Http\Resources\SimpleUserResource;
use App\Models\MerchantProduct;
use App\Models\Product;
use App\Models\User;
use App\Models\UserProduct;
use Dingo\Api\Http\Request;
/**
 * @group 用户模块
 */
class UserController extends MyController {
	/**
	 * 用户关注的产品
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function listMerchantProduct(Request $request) {
        $searchContent = $request->query('searchContent');
        $wxId = \auth()->user()->wx_id;
		$user = User::where(['wx_id' => $wxId])->firstOrFail();
        $merchantProducts = $user->merchantProducts ()
            ->with ( 'product' )
            ->with ( 'region' )
            ->get ();
        if ($searchContent) {
            $merchantProducts = $user->merchantProducts ()
                ->whereHas('product', function ($query) use ($searchContent) {
                    $query->where('name', 'like', '%'.$searchContent.'%')
                        ->orWhere('description', 'like', '%'.$searchContent.'%');
                })
                ->with ( 'product' )
                ->with ( 'region' )
                ->get ();
        }
		return UserMerchantProductResource::collection ( $merchantProducts );
	}

    /**
     * 未关注的其他产品
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getOtherProducts( Request $request) {
        $searchContent = $request->query('searchContent');
        $wxId = \auth()->user()->wx_id;
        $user = User::where(['wx_id' => $wxId])->firstOrFail();
        $merchantProducts = $user->merchantProducts ()->toArray();
        $merchantProductIds = array_column($merchantProducts, 'id');
        if ($searchContent) {
            $products = MerchantProduct::whereHas('product', function ($query) use ($searchContent) {
                $query->where('type',Product::COMMON_TYPE)
                    ->where(function ($whereQuery) use ($searchContent){
                        $whereQuery->where('name', 'like', '%'.$searchContent.'%')
                            ->orWhere('description', 'like', '%'.$searchContent.'%');
                    });
                })
                ->with('product')
                ->whereNotIn('id', $merchantProductIds)
                ->get();
        } else {
            $products = MerchantProduct::whereHas('product', function ($query) {
                $query->where('type', Product::COMMON_TYPE);
            })
                ->with('product')
                ->whereNotIn('id', $merchantProductIds)
                ->get();
        }
        return SimpleProductResource::collection ( $products );
    }
	/**
     * 关注产品
	 * @param int $merchant_product_id        	
	 * @return string
	 */
	public function addMerchantProduct($merchant_product_id) {
        $wxId = \auth()->user()->wx_id;
		$user = User::where(['wx_id' => $wxId])->firstOrFail();
		$user->merchantProducts ()->syncWithoutDetaching ( $merchant_product_id );
		return "success";
	}
	/**
     * 取消关注的产品
	 * @param int $merchant_product_id        	
	 * @return string
	 */
	public function deleteMerchantProduct($merchant_product_id) {
        $wxId = \auth()->user()->wx_id;
		$user = User::where(['wx_id' => $wxId])->firstOrFail();
		$user->merchantProducts ()->detach ( $merchant_product_id );
		return "success";
	}

    /**
     * 产品是否已关注
     * @param $merchant_product_id
     * @return string
     */
    public function isMyMerchantProduct($merchant_product_id) {
        $wxId = \auth()->user()->wx_id;
        $user = User::where(['wx_id' => $wxId])->firstOrFail();
        try {
            $userProduct = UserProduct::where(['user_id' => $user['id'], 'merchant_product_id' => $merchant_product_id])->firstOrFail();
            if ($userProduct && $userProduct->id) {
                return 'success';
            } else {
                return 'fail';
            }
        } catch (\Throwable $th) {
            return 'fail';
        }
        return 'fail';
    }
	/**
	 * 排序产品
     * 
	 * @param array $request
	 * @return string
	 */
	public function orderMerchantProduct(Request $request) {
        $orders = json_decode($request->getContent());
        $wxId = \auth()->user()->wx_id;
		$user = User::where(['wx_id' => $wxId])->firstOrFail();
		foreach ( $orders as $order ) {
			$userProduct = UserProduct::where(['user_id' => $user['id'], 'merchant_product_id' => $order->merchant_product_id])->firstOrFail();
            $userProduct->update([
                'order_number' => $order->order_number,
                'merchant_product_id' => $order->merchant_product_id
            ]);
		}
		return "success";
    }
    /**
     * 获取用户信息
     *
     * @return void
     */
    public function getUserInfo() {
        $wxId = \auth()->user()->wx_id;
        $user = User::where(['wx_id' => $wxId])->firstOrFail();
		return new SimpleUserResource ( $user );
    }

    /**
     * 推荐用户产品
     *
     * @param Request $request
     * @return void
     */
    public function recommendProducts(Request $request) {
        $wxId = \auth()->user()->wx_id;
		$user = User::where(['wx_id' => $wxId])->firstOrFail();
        $merchantProducts = $user->merchantProducts ()
            ->with ( 'product' )
            ->with ( 'region' )
            ->get ();
        if (empty($merchantProducts) || count($merchantProducts) == 0) {
            $merchantProducts = MerchantProduct::with ( 'product' )
            ->with ( 'region' )
            ->limit(10)
            ->get ();
        }
		return UserMerchantProductResource::collection ( $merchantProducts );
	}
}

<?php
/**
 * Created by PhpStorm.
 * User: chensf1
 * Date: 2018/7/28
 * Time: 13:53
 */

namespace App\Http\Api;


use App\Http\MyController;
use Dingo\Api\Routing\Helpers;

class AuthController extends MyController
{

    public function __construct()
    {
        $this->middleware('openapi', ['except' => ['login']]);
    }

    public function login()
    {
        $token = auth()->setTTL(env('TOKEN_TTL'))
                ->attempt([
                'username' => 'test-user',
                'wx_id' => '111111',
                'password' => env('DEFAUL_USER_PASSWORD')
            ]);
        if (! $token) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL()
        ]);
    }
}
<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * 
 * @property int $id
 * @property int $wx_id
 * @property string $real_name
 * @property string $username
 * @property string $info
 * @property Carbon $create_time
 *
 * @package App\Models
 */
class User extends Model
{
	protected $table = 'user';
	public $timestamps = false;

	protected $casts = [
		'wx_id' => 'int'
	];

	protected $dates = [
		'create_time'
	];

	protected $fillable = [
		'wx_id',
		'real_name',
		'username',
		'info',
		'create_time'
	];
}

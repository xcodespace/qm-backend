<?php

/**
 * Created by Jason.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectTimeLine
 * 
 * @property int $id
 * @property int $project_id
 * @property Carbon|null $application_date
 * @property Carbon|null $expected_launch_date
 * @property Carbon|null $acceptance_date
 * @property Carbon|null $plan_start_date
 * @property Carbon|null $plan_end_date
 * @property Carbon|null $actual_start_date
 * @property Carbon|null $actual_end_date
 * @property Carbon|null $charter_release_date
 * @property Carbon|null $project_start_date
 * @property Carbon|null $requirement_review_date
 * @property Carbon|null $requirement_release_date
 * @property Carbon|null $summary_design_review_date
 * @property Carbon|null $summary_design_release_date
 * @property Carbon|null $code_start_date
 * @property Carbon|null $code_end_date
 * @property Carbon|null $test_start_date
 * @property Carbon|null $test_end_date
 * @property Carbon|null $online_review_pass_date
 * @property Carbon|null $online_date
 * @property Carbon|null $deployment_complete date
 * @property Carbon|null $acceptance_passed_date
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class ProjectTimeLine extends Model
{
	protected $table = 'project_time_line';

	protected $casts = [
		'project_id' => 'int'
	];

	protected $dates = [
		'application_date',
		'expected_launch_date',
		'acceptance_date',
		'plan_start_date',
		'plan_end_date',
		'actual_start_date',
		'actual_end_date',
		'charter_release_date',
		'project_start_date',
		'requirement_review_date',
		'requirement_release_date',
		'summary_design_review_date',
		'summary_design_release_date',
		'code_start_date',
		'code_end_date',
		'test_start_date',
		'test_end_date',
		'online_review_pass_date',
		'online_date',
		'deployment_complete date',
		'acceptance_passed_date'
	];

	protected $fillable = [
	    'id',
		'project_id',
		'application_date',
		'expected_launch_date',
		'acceptance_date',
		'plan_start_date',
		'plan_end_date',
		'actual_start_date',
		'actual_end_date',
		'charter_release_date',
		'project_start_date',
		'requirement_review_date',
		'requirement_release_date',
		'summary_design_review_date',
		'summary_design_release_date',
		'code_start_date',
		'code_end_date',
		'test_start_date',
		'test_end_date',
		'online_review_pass_date',
		'online_date',
		'deployment_complete date',
		'acceptance_passed_date'
	];
}

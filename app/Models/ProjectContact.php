<?php

/**
 * Created by jason.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProjectContact
 * 
 * @property int $id
 * @property int $project_id
 * @property string|null $business_contact
 * @property string|null $tech_contact
 * @property string|null $leader_team
 * @property string|null $change_control_team
 * @property string|null $po
 * @property string|null $business_members
 * @property string|null $host_head
 * @property string|null $host_branch
 * @property string|null $co_head
 * @property string|null $co_branch
 * @property string|null $host_sa
 * @property string|null $pm
 * @property string|null $dev_members
 * @property string|null $lead_architect
 * @property string|null $data_architect
 * @property string|null $app_architect
 * @property string|null $tech_architect
 * @property string|null $security_architect
 * @property string|null $ptm
 * @property string|null $qa
 * @property string|null $data_platform_contact
 * @property string|null $app_platform_contact
 * @property string|null $product_app_maintenance_contact
 * @property string|null $product_env_protection_contact
 * @property string|null $product_net_protection_contact
 * @property string|null $project_tech_leader
 * @property string|null $config_admin
 * @property string|null $overall_structure_contact
 * @property string|null $tech_standard_contact
 * @property string|null $data_standard_contact
 * @property string|null $risk_related_contact
 * @property string|null $env_protection_contact
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class ProjectContact extends Model
{
	protected $table = 'project_contact';

	protected $casts = [
		'project_id' => 'int'
	];

	protected $fillable = [
	    'id',
		'project_id',
		'business_contact',
		'tech_contact',
		'leader_team',
		'change_control_team',
		'po',
		'business_members',
		'host_head',
		'host_branch',
		'co_head',
		'co_branch',
		'host_sa',
		'pm',
		'dev_members',
		'lead_architect',
		'data_architect',
		'app_architect',
		'tech_architect',
		'security_architect',
		'ptm',
		'qa',
		'data_platform_contact',
		'app_platform_contact',
		'product_app_maintenance_contact',
		'product_env_protection_contact',
		'product_net_protection_contact',
		'project_tech_leader',
		'config_admin',
		'overall_structure_contact',
		'tech_standard_contact',
		'data_standard_contact',
		'risk_related_contact',
		'env_protection_contact'
	];
}

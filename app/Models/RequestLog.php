<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Admin\RequestLog
 *
 * @property int $id
 * @property string $version API 版本
 * @property string $method HTTP 方法
 * @property string $uri 对应的路由
 * @property string $full_url 请求地址
 * @property mixed|null $queries 查询参数
 * @property mixed|null $headers 请求头部
 * @property string|null $user_agent 客户端
 * @property string $remote_addr 用户IP
 * @property mixed|null $parameters 路由参数
 * @property string|null $request_content 请求体
 * @property int $request_content_length 请求体大小
 * @property int|null $user_id 用户ID
 * @property string|null $response_content 响应内容
 * @property int $response_content_length 响应内容大小
 * @property int $status_code 状态码
 * @property string|null $analyzed_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RequestLog onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereAnalyzedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereFullUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereHeaders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereParameters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereQueries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereRemoteAddr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereRequestContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereRequestContentLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereResponseContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereResponseContentLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereStatusCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RequestLog whereVersion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RequestLog withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RequestLog withoutTrashed()
 * @mixin \Eloquent
 * @property-read \App\Models\User|null $user
 */
class RequestLog extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'version',
        'method',
        'uri',
        'full_url',
        'queries',
        'headers',
        'user_agent',
        'remote_addr',
        'parameters',
        'request_content',
        'request_content_length',
        'user_id',
        'response_content',
        'response_content_length',
        'status_code',
    ];

    protected $casts = [
        'queries' => 'array',
        'headers' => 'array',
        'parameters' => 'array',
        'analyzed_at' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}

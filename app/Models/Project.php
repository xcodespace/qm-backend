<?php

/**
 * Created by Jason.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Project
 * 
 * @property int $id
 * @property string $name
 * @property string $sequence
 * @property int $status
 * @property timestamp $innovation_year
 * @property int $stage
 * @property int $type
 * @property int $control_mode 研发掌控模式：1：自主研发；2：掌握研发；3：引进产品；4：完全外包
 * @property int $scale
 * @property int $scope
 * @property int $dev_mode 1：敏捷，2：传统
 * @property string $competent_authority 业务主管部门
 * @property string $classify_name
 * @property string $budget_type
 * @property string $dev_unit
 * @property string $dev_team
 * @property string $sys_user
 * @property int $demand_dev_mode
 * @property int $test_mode
 * @property string $target
 * @property string $description
 * @property string $remark
 * @property string $main_sys
 * @property string $related_sys
 * @property timestamp|null $created_at
 * @property timestamp|null $updated_at
 *
 * @package App\Models
 */
class Project extends Model
{
	protected $table = 'project';

	protected $casts = [
		'status' => 'int',
		'stage' => 'int',
		'type' => 'int',
		'control_mode' => 'int',
		'scale' => 'int',
		'scope' => 'int',
		'dev_mode' => 'int',
		'demand_dev_mode' => 'int',
		'test_mode' => 'int'
	];

	protected $dates = [
		'innovation_year'
	];

	protected $fillable = [
	    'id',
		'name',
		'sequence',
		'status',
		'innovation_year',
		'stage',
		'type',
		'control_mode',
		'scale',
		'scope',
		'dev_mode',
		'competent_authority',
		'classify_name',
		'budget_type',
		'dev_unit',
		'dev_team',
		'sys_user',
		'demand_dev_mode',
		'test_mode',
		'target',
		'description',
		'remark',
		'main_sys',
		'related_sys'
	];
	// 状态定义
    const STATUS_DOING = 1;
    const STATUS_STOP = 2;
    const STATUS_PRODUCED = 3;
    public static $statusMap = [
        self::STATUS_DOING => '在建',
        self::STATUS_STOP => '暂停',
        self::STATUS_PRODUCED => '已投产',
    ];
    public static function statusVal($statusName){
        $reverseMap = array_flip(self::$statusMap);
        return isset($reverseMap[$statusName]) ? $reverseMap[$statusName] : 0;
    }
    // 阶段定义
    const STAGE_REQUIREMENT = 1;
    const STAGE_START = 2;
    const STAGE_OUTLINE_DESIGN = 3;
    const STAGE_CODING =4;
    const STAGE_TESTING = 5;
    const STAGE_ONLINE_PRE = 6;
    const STAGE_ONLINE_SUC = 7;
    public static $stageMap = [
        self::STAGE_REQUIREMENT => '软件需求',
        self::STAGE_START => '启动',
        self::STAGE_OUTLINE_DESIGN => '概要设计',
        self::STAGE_CODING => '编码',
        self::STAGE_TESTING => '测试',
        self::STAGE_ONLINE_PRE => '上线准备',
        self::STAGE_ONLINE_SUC => '上线成功'
    ];
    public static function stageVal($label){
        $reverseMap = array_flip(self::$stageMap);
        return isset($reverseMap[$label]) ? $reverseMap[$label] : 0;
    }

    // 掌控模式
    const CONTROL_MODE_SELF = 1;
    const CONTROL_MODE_CONTROL = 2;
    const CONTROL_MODE_IMPORT = 3;
    const CONTROL_MODE_OTHER = 4;

    public static $controlModeMap = [
        self::CONTROL_MODE_SELF => '自主研发',
        self::CONTROL_MODE_CONTROL => '掌握研发',
        self::CONTROL_MODE_IMPORT => '引进产品',
        self::CONTROL_MODE_OTHER => '完全外包'
    ];
    public static function controlModeVal($label) {
        $reverseMap = array_flip(self::$controlModeMap);
        return isset($reverseMap[$label]) ? $reverseMap[$label] : 0;
    }

    const DEV_MODE_COMMON = 1;
    const DEV_MODE_AGILE = 2;
    public static $devModeMap = [
        self::DEV_MODE_COMMON => '传统模式',
        self::DEV_MODE_AGILE => '敏捷模式'
    ];
    public static function devModeVal($label) {
        $reverseMap = array_flip(self::$devModeMap);
        return isset($reverseMap[$label]) ? $reverseMap[$label] : 0;
    }

    // 项目规模
    const SCALE_A1 = 1;
    const SCALE_A2 = 2;
    const SCALE_A3 = 3;
    const SCALE_AA1 = 4;
    const SCALE_AA2 = 5;
    const SCALE_B1 = 6;
    const SCALE_B2 = 7;
    const SCALE_B3 = 8;
    const SCALE_C1 = 9;
    const SCALE_C2 = 10;
    const SCALE_C3 = 11;
    const SCALE_D1 = 12;
    const SCALE_D2 = 13;
    const SCALE_D3 = 14;
    const SCALE_D4 = 15;
    public static $scaleMap = [
        self::SCALE_A1 => 'A1',
        self::SCALE_A2 => 'A2',
        self::SCALE_A3 => 'A3',
        self::SCALE_AA1 => 'AA1',
        self::SCALE_AA2 => 'AA2',
        self::SCALE_B1 => 'B1',
        self::SCALE_B2 => 'B2',
        self::SCALE_B3 => 'B3',
        self::SCALE_C1 => 'C1',
        self::SCALE_C2 => 'C2',
        self::SCALE_C3 => 'C3',
        self::SCALE_D1 => 'D1',
        self::SCALE_D2 => 'D2',
        self::SCALE_D3 => 'D3',
        self::SCALE_D4 => 'D4',
    ];
    public static function scaleVal($label) {
        $reverseMap = array_flip(self::$scaleMap);
        return isset($reverseMap[$label]) ? $reverseMap[$label] : 0;
    }

    // 掌控模式
    const TEST_MODE_SELF = 1;
    const TEST_MODE_OTHER = 2;
    const TEST_MODE_PERFORMANCE = 3;
    const TEST_MODE_SPECIAL = 4;
    public static $testModeMap = [
        self::TEST_MODE_SELF => '自主测试',
        self::TEST_MODE_OTHER => '外包测试',
        self::TEST_MODE_PERFORMANCE => '性能测试',
        self::TEST_MODE_SPECIAL => '专项测试'
    ];
    public static function testModeVal($label) {
        $reverseMap = array_flip(self::$testModeMap);
        return isset($reverseMap[$label]) ? $reverseMap[$label] : 0;
    }

    // 项目类型
    const TYPE_MANAGEMENT = 1;
    const TYPE_FINANCIAL = 2;
    const TYPE_BASIC_PLATFORM = 3;
    public static $typeMap = [
        self::TYPE_MANAGEMENT => '管理信息',
        self::TYPE_FINANCIAL => '金融产品',
        self::TYPE_BASIC_PLATFORM => '基础平台',
    ];
    public static function typeVal($label){
        $reverseMap = array_flip(self::$typeMap);
        return isset($reverseMap[$label]) ? $reverseMap[$label] : 0;
    }

    // 应用范围
    const SCOPE_HOST_AND_BRANCH = 1;
    const SCOPE_HOST = 2;
    const SCOPE_BRANCH = 3;
    public static $scopeMap = [
        self::SCOPE_HOST_AND_BRANCH => '总行&分行',
        self::SCOPE_HOST => '总行',
        self::SCOPE_BRANCH => '分行'
    ];
    public static function scopeVal($label) {
        $reverseMap = array_flip(self::$controlModeMap);
        return isset($reverseMap[$label]) ? $reverseMap[$label] : 0;
    }

    // 需求开发模式
    const DEMAND_DEV_MODE_REDIRECT = 1;
    const DEMAND_DEV_MODE_DELEGATE = 2;
    public static $demandDevModeMap = [
        self::DEMAND_DEV_MODE_REDIRECT => '直接参与',
        self::DEMAND_DEV_MODE_DELEGATE => '委派制'
    ];
    public static function demandDevModeVal($label) {
        $reverseMap = array_flip(self::$demandDevModeMap);
        return isset($reverseMap[$label]) ? $reverseMap[$label] : 0;
    }

    public function timeLine() {
        return $this->hasOne('App\Models\ProjectTimeLine','project_id','id');
    }

    public function contact() {
        return $this->hasOne('App\Models\ProjectContact','project_id','id');
    }


}

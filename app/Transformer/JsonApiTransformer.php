<?php
/**
 * Created by PhpStorm.
 * User: chensf1
 * Date: 2018/8/1
 * Time: 21:04
 */

namespace App\Transformer;

use App\Serializer\JsonApiSerializer;
use Dingo\Api\Transformer\Adapter\Fractal;
use League\Fractal\Manager as FractalManager;

class JsonApiTransformer extends Fractal
{
    public function __construct(FractalManager $fractal, string $includeKey = 'include', string $includeSeparator = ',', bool $eagerLoading = true)
    {
        $fractal->setSerializer(new JsonApiSerializer);
        parent::__construct($fractal, $includeKey, $includeSeparator, $eagerLoading);
    }

}
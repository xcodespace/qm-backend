<?php
/**
 * Created by PhpStorm.
 * User: chensf1
 * Date: 2018/8/12
 * Time: 23:18
 */

namespace App\Transformer;


use Illuminate\Contracts\Support\Arrayable;
use League\Fractal\TransformerAbstract;

class BasicModelTransformer extends TransformerAbstract
{
    public function transform(Arrayable $model)
    {
        return $model->toArray();
    }
}
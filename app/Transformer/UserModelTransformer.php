<?php
/**
 * Created by PhpStorm.
 * User: chensf1
 * Date: 2018/8/20
 * Time: 23:45
 */

namespace App\Transformer;


use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserModelTransformer extends TransformerAbstract
{
    public function transform(User $model)
    {
        $user = $model->toArray();
        $org = $model->organizations()->whereKey($user['active_org_id'])->first();
        $user['role'] = $org['roles']['role'];
        unset($user['relation']['organization_id']);
        unset($user['relation']['user_id']);
        return $user;
    }
}
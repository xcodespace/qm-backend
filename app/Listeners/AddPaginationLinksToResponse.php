<?php
/**
 * Created by PhpStorm.
 * User: chensf1
 * Date: 2018/8/9
 * Time: 15:42
 */

namespace App\Listeners;


use Dingo\Api\Event\ResponseWasMorphed;

class AddPaginationLinksToResponse
{
    public function handle(ResponseWasMorphed $event)
    {
        // Link 转到头部
        if (is_array($event->content) && isset($event->content['meta']) && array_key_exists('Link', $event->content['meta']))
        {
            $headerLink = [];
            foreach ($event->content['meta']['Link'] as $item)
            {
                $headerLink[] = sprintf('<%s>; rel="%s"', $item[0], $item[1]['rel']);
            }

            $event->response->headers->set('Link', implode(',', $headerLink));

            unset($event->content['meta']['Link']);
        }
    }
}
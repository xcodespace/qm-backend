<?php
/**
 * Created by PhpStorm.
 * User: chensf1
 * Date: 2018/8/1
 * Time: 19:10
 */

namespace App\Serializer;


use League\Fractal\Pagination\PaginatorInterface;
use League\Fractal\Serializer\ArraySerializer;

class JsonApiSerializer extends ArraySerializer
{
    protected $baseUrl;
    protected $rootObjects;

    public function item($resourceKey, array $data)
    {
        return ['data' => $data];
    }


    public function collection($resourceKey, array $data)
    {
        return ['data' => $data];
    }

    public function paginator(PaginatorInterface $paginator)
    {
        $currentPage = (int)$paginator->getCurrentPage();

        $lastPage = (int)$paginator->getLastPage();

        $nextPage = $lastPage < $currentPage + 1 ? $lastPage : $currentPage + 1;

        $links = [
            [
                $paginator->getUrl($nextPage),
                ['rel' => 'next']
            ], [
                $paginator->getUrl($lastPage),
                ['rel' => 'last']
            ], [
                $paginator->getUrl(1),
                ['rel' => 'first']
            ], [
                $paginator->getUrl($currentPage - 1),
                ['rel' => 'prev']
            ],
        ];

        return ['Link' => $links];

    }

    public function meta(array $meta)
    {
        if (empty($meta))
        {
            return ['meta' => []];
        }

        $result['meta'] = $meta;

        return $result;
    }

    public function null()
    {
        return ['data' => []];
    }

}
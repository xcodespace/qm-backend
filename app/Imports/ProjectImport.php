<?php

namespace App\Imports;

use App\Models\Project;
use App\Models\ProjectTimeLine;
use App\Models\ProjectContact;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Log;

class ProjectImport implements ToCollection
{
    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $index => $row) {
            Log::info($row);
            if ($index === 0) {
                continue;
            }
            if (empty($row[1])) {
                return false;
            }
            $project = Project::where('name', $row[1])->first();
            // 增加项目基础信息
            if (empty($project)) {
                $project = Project::create([
                    'dev_mode' => Project::DEV_MODE_COMMON,
                    'name' => $row[1],
                    'sequence' => $row[2],
                    'status' => Project::statusVal($row[3]),
                    'innovation_year' => !empty($row[4]) ? $row[4] : null,
                    'stage' => Project::stageVal($row[5]),
                    'type' => Project::typeVal($row[6]),
                    // 该字段作为研发模式使用，废弃掌控模式
//                    'control_mode' => Project::controlModeVal($row[7]),
                    'scale' => Project::scaleVal($row[8]),
                    'scope' => Project::scaleVal($row[10]),
                    'competent_authority' => $row[11],
                    'budget_type' => $row[20],
                    'classify_name' => $row[21],
                    'dev_unit' => $row[22],
                    'dev_team' => $row[23],
                    'demand_dev_mode' => Project::demandDevModeVal($row[24]),
                    'test_mode' => Project::testModeVal($row[25]),
                    'target' => $row[26],
                    'description' => $row[27],
                    'sys_user' => $row[28],
                    'remark' => $row[29],
                    'main_sys' => $row[30],
                    'related_sys' => $row[31],
                ]);
            } else {
                $project->save();
            }
            $projectId = $project->id;

            // 增加项目时间字段
            $projectTimeLine = ProjectTimeLine::where('project_id', $projectId)->first();
            if(empty($projectTimeLine)) {
                $projectTimeLine = ProjectTimeLine::create([
                    'project_id' => $projectId,
                    'application_date' => !empty($row[9]) ? $row[9] : null,
                    'expected_launch_date' => !empty($row[14]) ? $row[14] : null,
                    'acceptance_date' => !empty($row[15]) ? $row[15] : null,
                    'plan_start_date' => !empty($row[16]) ? $row[16] : null,
                    'plan_end_date' => !empty($row[17]) ? $row[17] : null,
                    'actual_start_date' => !empty($row[18]) ? $row[18] : null,
                    'actual_end_date' => !empty($row[19]) ? $row[19] : null,
                    'charter_release_date' => !empty($row[62]) ? $row[62] : null,
                    'project_start_date' => !empty($row[63]) ? $row[63] : null,
                    'requirement_review_date' => !empty($row[64]) ? $row[64] : null,
                    'requirement_release_date' => !empty($row[65]) ? $row[65] : null,
                    'summary_design_review_date' => !empty($row[66]) ? $row[66] : null,
                    'summary_design_release_date' => !empty($row[67]) ? $row[67] : null,
                    'code_start_date' => !empty($row[68]) ? $row[68] : null,
                    'code_end_date' => !empty($row[69]) ? $row[69] : null,
                    'test_start_date' => !empty($row[70]) ? $row[70] : null,
                    'test_end_date' => !empty($row[71]) ? $row[71] : null,
                    'online_review_pass_date' => !empty($row[72]) ? $row[72] : null,
                    'online_date' => !empty($row[73]) ? $row[73] : null,
                    'deployment_complete date' => !empty($row[74]) ? $row[74] : null,
                    'acceptance_passed_date' => !empty($row[75]) ? $row[75] : null,
                ]);
            } else {
                $projectTimeLine->save();
            }

            // 项目联系人信息
            $projectContact = ProjectContact::where('project_id', $projectId)->first();
            if(empty($projectContact)) {
                $projectContact = ProjectContact::create([
                    'project_id' => $projectId,
                    'business_contact' => $row[12],
                    'tech_contact' => $row[13],
                    'leader_team' => $row[32],
                    'change_control_team' => $row[33],
                    'po' => $row[34],
                    'business_members' => $row[35],
                    'host_head' => $row[36],
                    'host_branch' => $row[37],
                    'co_head' => $row[38],
                    'co_branch' => $row[39],
                    'host_sa' => $row[40],
                    'pm' => $row[41],
                    'dev_members' => $row[42],
                    'lead_architect' => $row[43],
                    'data_architect' => $row[44],
                    'app_architect' => $row[45],
                    'tech_architect' => $row[46],
                    'security_architect' => $row[47],
                    'ptm' => $row[48],
                    'qa' => $row[49],
                    'data_platform_contact' => $row[50],
                    'app_platform_contact' => $row[51],
                    'product_app_maintenance_contact' => $row[52],
                    'product_env_protection_contact' => $row[53],
                    'product_net_protection_contact' => $row[54],
                    'project_tech_leader' => $row[55],
                    'config_admin' => $row[56],
                    'overall_structure_contact' => $row[57],
                    'tech_standard_contact' => $row[58],
                    'data_standard_contact' => $row[59],
                    'risk_related_contact' => $row[60],
                    'env_protection_contact' => $row[61]
                ]);
            } else {
                $projectContact->save();
            }

        }
    }
}

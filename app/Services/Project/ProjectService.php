<?php
/**
 * Created by PhpStorm.
 * User: wangjiansheng
 * Date: 2020/10/19
 * Time: 6:00 PM
 */

namespace App\Services\Project;


use App\Models\Project;
use App\Models\ProjectContact;
use Illuminate\Support\Facades\DB;

class ProjectService
{
    /**
     * 项目列表
     * @param string $name
     * @param string $qa
     * @param string $pm
     * @param int $scale
     * @param int $controlMode
     * @param int $risk
     * @param int $perPage
     * @return mixed
     */
    public function searchProject(
        $name = '',
        $qa = '',
        $pm = '',
        $scale = 0,
        $controlMode = 0,
        $risk = 0,
        $perPage = 10
    ) {
        $queryBuilder = Project::query()->with(['timeLine', 'contact']);

        // 项目名称查询
        $queryBuilder = !empty($name) ? $queryBuilder->where('name', 'LIKE', '%' . $name . '%') : $queryBuilder;

        // qa查询
        $queryBuilder = !empty($qa) ? $queryBuilder->where('contact.qa', $qa) : $queryBuilder;

        // 项目经理名称查询
        $queryBuilder = !empty($pm) ? $queryBuilder->where('contact.pm', 'LIKE', '%' . $pm . '%') : $queryBuilder;

        // 项目规模
        $queryBuilder = !empty($scale) ? $queryBuilder->where('scale', $scale) : $queryBuilder;

        // 掌控模式
        $queryBuilder = !empty($controlMode) ? $queryBuilder->where('control_mode', $controlMode) : $queryBuilder;

        // 风险等级
        $queryBuilder = !empty($risk) ? $queryBuilder->where('risk', $risk) : $queryBuilder;

        return $queryBuilder->paginate($perPage);
    }

    /**
     * 获取项目详情
     * @param $id
     * @return mixed
     */
    public function getProjectById($id) {
        $project = Project::find($id);
        $project->timeLine = $project->timeLine;
        $project->contact = $project->contact;
        return $project;
    }

    /**
     * 根据项目规模统计项目数量
     * @return \Illuminate\Support\Collection
     */
    public function getProjectCountByScale() {
        $result = DB::table('project')
            ->select('scale', DB::raw('count(*) as count'))
            ->groupBy('scale')
            ->orderBy('scale')
            ->get();
        foreach ($result as $item) {
            $item->scale_name = isset(Project::$scaleMap[$item->scale]) ? Project::$scaleMap[$item->scale] : '未知';
        }
        return $result;
    }

    /**
     * 项目类型统计
     * @return \Illuminate\Support\Collection
     */
    public function getProjectCountByDevMode() {
        $result = DB::table('project')
            ->select('dev_mode', DB::raw('count(*) as count'))
            ->groupBy('dev_mode')
            ->orderBy('dev_mode')
            ->get();
        foreach ($result as $item) {
            $item->dev_mode_name = isset(Project::$devModeMap[$item->dev_mode]) ? Project::$devModeMap[$item->dev_mode] : '未知';
        }
        return $result;
    }

    /**
     * 项目研发模式统计
     * @return \Illuminate\Support\Collection
     */
    public function getProjectCountByControlMode() {
        $result = DB::table('project')
            ->select('control_mode', DB::raw('count(*) as count'))
            ->groupBy('control_mode')
            ->orderBy('control_mode')
            ->get();
        foreach ($result as $item) {
            $item->control_mode_name = isset(Project::$controlModeMap[$item->control_mode]) ? Project::$controlModeMap[$item->control_mode] : '未知';
        }
        return $result;
    }

    /**
     * 项目阶段统计
     * @return \Illuminate\Support\Collection
     */
    public function getProjectCountByStage() {
        $result = DB::table('project')
            ->select('stage', DB::raw('count(*) as count'))
            ->groupBy('stage')
            ->orderBy('stage')
            ->get();
        foreach ($result as $item) {
            $item->stage_name = isset(Project::$stageMap[$item->stage]) ? Project::$stageMap[$item->stage] : '未知';
        }
        return $result;
    }

    /**
     * 项目QA统计，按人员统计
     * @return \Illuminate\Support\Collection
     */
    public function getProjectCountByQa() {
        $result = DB::table('project')
            ->leftJoin('project_contact','project.id', '=', 'project_contact.project_id')
            ->select('project_contact.qa as qa', DB::raw('count(*) as count'))
            ->groupBy('qa')
            ->get();
        return $result;
    }

    /**
     * 统计项目总数、人员总数、风险总数等
     * @return array
     */
    public function getProjectTotalCount() {
        $projectCount = Project::count();
        $qaCount = ProjectContact::count()->groupBy('qa');
        // TODO 风险项目汇总
        $riskCount = 0;
        return array(
            'project_count' => $projectCount,
            'qa_count' => $qaCount,
            'risk_count' => $riskCount
        );
    }




}
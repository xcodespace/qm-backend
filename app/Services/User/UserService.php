<?php
/**
 * Created by PhpStorm.
 * User: wangjiansheng
 * Date: 2020/2/8
 * Time: 1:54 PM
 */

namespace App\Services\User;


use App\Models\User;

class UserService
{


    public function deleteUserById($id) {
        return User::where('id', $id)->delete();
    }

}
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\User;

$controller = app('Dingo\Api\Routing\Router');
Route::get('/test', function () {
    return view('welcome');
});

/**
 * Wechat Routes
 */
Route::group(['middleware' => 'mock.user'], function () {//这个中间件可以先忽略，我们稍后再说
    Route::middleware('wechat.oauth:snsapi_base')->group(function () {
        Route::get('/login', 'SelfAuthController@autoLogin')->name('login');
    });
    Route::middleware('wechat.oauth:snsapi_userinfo')->group(function () {
        Route::get('/register', 'SelfAuthController@autoRegister')->name('register');
    });
});

Route::any('/wechat', 'WechatController@serve');
Route::get('/wechat/createMenu', 'WechatController@createMenu');
Route::group(['middleware' => ['web', 'wechat.oauth']], function () {
    Route::get('/user', function () {
        $userInfo = session('wechat.oauth_user.default'); // 拿到授权用户资料
        $openId = $userInfo['id'];
        //查看对应的openid是否已被注册
        $userModel = User::where('wx_id', $openId)->first();
        if(!$userModel) {
            //根据微信信息注册用户。
            $userData = [
                'wx_id' => $userInfo['id'],
                'username' => $userInfo['nickname'],
                'password' => env('DEFAUL_USER_PASSWORD'),
                'avatar' => $userInfo['avatar'],
                'info' => json_encode($userInfo['original'])
            ];
            //注意批量写入需要把相应的字段写入User中的$fillable属性数组中
            User::create($userData);
        } else {
            $userModel->wx_id = $userInfo['id'];
            $userModel->username = $userInfo['nickname'];
            $userModel->password = env('DEFAUL_USER_PASSWORD');
            $userModel->avatar = $userInfo['avatar'];
            $userModel->info = json_encode($userInfo['original']);
            $userModel->save();
        }

        // 生成token
        $token = auth()->setTTL(env('TOKEN_TTL'))->attempt([
            'username' => $userInfo['nickname'],
            'wx_id' => $userInfo['id'],
            'password' => env('DEFAUL_USER_PASSWORD')
        ]);
        header('location:http://app.shareiron.com?openid='.$userInfo['id'].'&username='.$userInfo['nickname'].'&access_token='.$token.'&expires_in='.env('TOKEN_TTL')*60);
    });
    Route::get('/shop', function () {
        $userInfo = session('wechat.oauth_user.default'); // 拿到授权用户资料
        $openId = $userInfo['id'];
        //查看对应的openid是否已被注册
        $userModel = User::where('wx_id', $openId)->first();
        if(!$userModel) {
            //根据微信信息注册用户。
            $userData = [
                'wx_id' => $userInfo['id'],
                'username' => $userInfo['nickname'],
                'password' => env('DEFAUL_USER_PASSWORD'),
                'avatar' => $userInfo['avatar'],
                'type' => 0
            ];
            //注意批量写入需要把相应的字段写入User中的$fillable属性数组中
            User::create($userData);
        } else {
            $userModel->wx_id = $userInfo['id'];
            $userModel->username = $userInfo['nickname'];
            $userModel->password = env('DEFAUL_USER_PASSWORD');
            $userModel->avatar = $userInfo['avatar'];
            $userModel->type = 0;
            $userModel->save();
        }

        // 生成token
        $token = auth()->setTTL(env('TOKEN_TTL'))->attempt([
            'username' => $userInfo['nickname'],
            'wx_id' => $userInfo['id'],
            'password' => env('DEFAUL_USER_PASSWORD')
        ]);
        header('location:http://shop.shareiron.com?openid='.$userInfo['id'].'&username='.$userInfo['nickname'].'&access_token='.$token.'&expires_in='.env('TOKEN_TTL')*60);
    });
});

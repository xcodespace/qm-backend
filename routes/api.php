<?php

use App\Models\ProductPrice;

// Route::post('refreshToken',function(){
//     $token = auth()->setTTL(env('TOKEN_TTL'))
//         ->attempt([
//         'username' => 'test-user',
//         'password' => env('DEFAUL_USER_PASSWORD')
//     ]);
//     return array(
//         'token' => $token,
//         'expire_time' => env('TOKEN_TTL')
//     ); 
// });

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'App\Http\Api', 'middleware' => 'openapi'], function ()
{
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/me', 'AuthController@me');
    Route::post('auth/refresh', 'AuthController@refresh');

    Route::get('test',function(){
    	return ProductPrice::all();
    });
});

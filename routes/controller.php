<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$controller = app('Dingo\Api\Routing\Router');

$controller->version('v1', function ($controller)
{
    $controller->get('/', 'App\Http\Controller\IndexController@index');

    // 登录、注册
    $controller->group(['namespace' => 'App\Http\Controller\Auth'], function ($controller)
    {
        $controller->post('login', 'LoginController@login');
        $controller->post('logout', 'LoginController@logout');
        $controller->post('register', 'RegisterController@register');
    });
    // 用户
    $controller->group(['namespace' => 'App\Http\Controller'], function ($controller)
    {
        $controller->get('user/info', 'UsersController@info');
        $controller->get('user/list', 'UsersController@list');
        $controller->get('user/delete/{uid}', 'UsersController@delete');
        $controller->get('user/trend', 'UsersController@getUserTrend');
    });

    // 项目管理
    $controller->group(['namespace' => 'App\Http\Controller'], function ($controller)
    {
        $controller->get('project/info', 'ProjectController@info');
        $controller->get('project/list', 'ProjectController@list');
        $controller->get('project/scaleStatistics', 'ProjectController@scaleStatistics');
        $controller->get('project/devModeStatistics', 'ProjectController@devModeStatistics');
        $controller->get('project/controlModeStatistics', 'ProjectController@controlModeStatistics');
        $controller->get('project/stageStatistics', 'ProjectController@stageStatistics');
        $controller->get('project/qaStatistics', 'ProjectController@qaStatistics');
    });

    // 上传文件
    $controller->group(['namespace' => 'App\Http\Controller'], function ($controller)
    {
        $controller->post('upload/upload', 'UploadController@upload');
    });
});
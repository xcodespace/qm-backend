镜像环境：ubuntu + nginx + php72
nginx 配置 cms.conf 拷贝至 /etc/nginx/conf.d/cms.conf ，映射端口 8080
项目代码 挂载在 容器内的 /var/www/html 下
docker build -t cms:1.0 -f build/Dockerfile .
docker run -idt --name cms-api-docker -p 8081:8081 -v /var/www/cms/app:/var/www/html/app -v /var/www/cms/database:/var/www/html/database -v /var/www/cms/routes:/var/www/html/routes -v /var/www/cms/storage:/var/www/html/storage 214956be25d9
docker exec -it {docker-container-id} bash -c "php artisan migrate"
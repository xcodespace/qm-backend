<?php
use App\Models\MerchantProduct;
use App\Models\User;
use Illuminate\Database\Seeder;
class UserTableSeeder extends Seeder {
	
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$users = factory ( User::class, 20 )->create ()->each ( function ($user) {
			$merchantProducts = MerchantProduct::all ()->random ( rand ( 0, 5 ) );
			for($i = 0; $i < $merchantProducts->count (); $i ++) {
				$user->merchantProducts ()->save ( $merchantProducts [$i], [ 
						'order_number' => $i + 1 
				] );
			}
		} );
	}
}

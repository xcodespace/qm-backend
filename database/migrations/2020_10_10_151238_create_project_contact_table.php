<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_contact', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->comment('项目ID');
            $table->string('business_contact',32)->nullable()->comment('业务联系人');
            $table->string('tech_contact',32)->nullable()->comment('科技联系人');
            $table->string('leader_team',128)->nullable()->comment('领导小组');
            $table->string('change_control_team',64)->nullable()->comment('变更控制小组');
            $table->string('po',32)->nullable()->comment('产品经理');
            $table->string('business_members',512)->nullable()->comment('业务成员');
            $table->string('host_head',128)->nullable()->comment('主办（总行）');
            $table->string('host_branch',128)->nullable()->comment('主办（分行）');
            $table->string('co_head',128)->nullable()->comment('协办（总行）');
            $table->string('co_branch',128)->nullable()->comment('协办（分行）');
            $table->string('host_sa',32)->nullable()->comment('主办（SA)');
            $table->string('pm',32)->nullable()->comment('项目经理');
            $table->string('dev_members',256)->nullable()->comment('研发成员');
            $table->string('lead_architect',32)->nullable()->comment('领衔架构师');
            $table->string('data_architect',32)->nullable()->comment('数据架构师');
            $table->string('app_architect',32)->nullable()->comment('应用架构师');
            $table->string('tech_architect',32)->nullable()->comment('技术架构师');
            $table->string('security_architect',32)->nullable()->comment('安全架构师');
            $table->string('ptm',32)->nullable()->comment('测试经理');
            $table->string('qa',32)->nullable()->comment('质量控制人员');
            $table->string('data_platform_contact',32)->nullable()->comment('数据平台联系人');
            $table->string('app_platform_contact',32)->nullable()->comment('应用平台联系人');
            $table->string('product_app_maintenance_contact',32)->nullable()->comment('生产应用维护联系人');
            $table->string('product_env_protection_contact',32)->nullable()->comment('生产环境保障联系人');
            $table->string('product_net_protection_contact',128)->nullable()->comment('生产网络保障联系人');
            $table->string('project_tech_leader',32)->nullable()->comment('项目技术负责人');
            $table->string('config_admin',32)->nullable()->comment('配置管理员');
            $table->string('overall_structure_contact',32)->nullable()->comment('总体架构联系人');
            $table->string('tech_standard_contact',32)->nullable()->comment('技术标准联系人');
            $table->string('data_standard_contact',32)->nullable()->comment('数据标准联系人');
            $table->string('risk_related_contact',32)->nullable()->comment('风险管理联系人');
            $table->string('env_protection_contact',32)->nullable()->comment('环境保障联系人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_contact');
    }
}

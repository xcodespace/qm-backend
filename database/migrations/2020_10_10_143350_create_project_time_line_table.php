<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTimeLineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_time_line', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->comment('项目ID');
            $table->date('application_date')->nullable()->comment('申请日期');
            $table->date('expected_launch_date')->nullable()->comment('期望上线日期');
            $table->date('acceptance_date')->nullable()->comment('受理日期');
            $table->date('plan_start_date')->nullable()->comment('计划开始日期');
            $table->date('plan_end_date')->nullable()->comment('计划结束日期');
            $table->date('actual_start_date')->nullable()->comment('实际开始日期');
            $table->date('actual_end_date')->nullable()->comment('实际结束日期');
            $table->date('charter_release_date')->nullable()->comment('章程发布日期');
            $table->date('project_start_date')->nullable()->comment('项目启动日期');
            $table->date('requirement_review_date')->nullable()->comment('需求评审日期');
            $table->date('requirement_release_date')->nullable()->comment('需求发布日期');
            $table->date('summary_design_review_date')->nullable()->comment('概要设计评审日期');
            $table->date('summary_design_release_date')->nullable()->comment('概要设计发布日期');
            $table->date('code_start_date')->nullable()->comment('编码开始日期');
            $table->date('code_end_date')->nullable()->comment('编码结束日期');
            $table->date('test_start_date')->nullable()->comment('测试开始日期');
            $table->date('test_end_date')->nullable()->comment('测试结束日期');
            $table->date('online_review_pass_date')->nullable()->comment('上线评审通过日期');
            $table->date('online_date')->nullable()->comment('上线日期');
            $table->date('deployment_complete date')->nullable()->comment('部署完成日期');
            $table->date('acceptance_passed_date')->nullable()->comment('验收通过日期');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_time_line');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',128)->comment('项目名称');
            $table->string('sequence',64)->nullable()->comment('项目编号');
            $table->unsignedTinyInteger('status')->comment('项目状态');
            $table->string('innovation_year',32)->nullable()->comment('创新年度');
            $table->unsignedTinyInteger('stage')->comment('项目阶段');
            $table->unsignedTinyInteger('type')->comment('项目类型');
            $table->unsignedTinyInteger('control_mode')->comment('管控模式');
            $table->unsignedTinyInteger('scale')->comment('项目规模');
            $table->unsignedTinyInteger('scope')->comment('应用范围:分行、总行、总行&分行');
            $table->unsignedTinyInteger('dev_mode')->comment('开发模式:敏捷开发、传统开发');
            $table->string('competent_authority',64)->comment('主管部门');
            $table->string('classify_name',64)->comment('项目分类：创新项目、普通项目');
            $table->string('budget_type',128)->comment('IT预算事项类型');
            $table->string('dev_unit',128)->comment('研发单位');
            $table->string('dev_team',128)->comment('研发团队');
            $table->string('sys_user',128)->comment('系统用户');
            $table->unsignedTinyInteger('demand_dev_mode')->comment('需求开发参与模式:委派制、直接参与');
            $table->unsignedTinyInteger('test_mode')->comment('测试模式:外包测试、性能专项、专业测试、自主测试');
            $table->text('target')->comment('项目目标');
            $table->text('description')->comment('需求综述');
            $table->text('remark')->comment('备注');
            $table->string('main_sys',128)->comment('主系统');
            $table->string('related_sys',512)->comment('配套系统');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}

# Laravel + Dingo + JWT

## 文档链接

JWT: [https://github.com/tymondesigns/jwt-auth/wiki/Installation](https://github.com/tymondesigns/jwt-auth/wiki/Installation)

Dingo: [https://github.com/dingo/api/wiki/Installation](https://github.com/dingo/api/wiki/Installation)

## 配置指南

### 初始化环境
```
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
composer install
```

### JWT
密钥需要自己生成。命令为：`php artisan jwt:secret`

### Dingo
需要配置 `.env` 文件，从 `.env.example` 复制一份，再配置。

### 初始化项目

```
php artisan migrate
php artisan db:seed --class=BouncerSeeder
```

### 项目部署
```
cp -rf ../api-service-bak/.env  ./  
cp -rf ../api-service-bak/config/database.php  ./config/
chmod 777 storage/*
```

### CI/CD部署1
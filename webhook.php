<?php

/**
 * GITEE HOOK 服务器端代码
 *
 */


$git = "git"; //默认是用git全局变量，有的环境可能要指明具体安装路径
$branch = "develop"; //pull分支，为空就是默认分支
$logName = "git_webhook_log"; //本地日志名称，与当前php文件在同一目录
$savePath = "/webdata/xmchat/cms"; //项目根目录，初次克隆确保目录为空
$gitSSHPath  = "git@gitee.com:xd_team/cms.git";//代码仓库SSH地址
$password = "xmecho"; //在GITEE设置的密码
$is_test = false;//测试模式，无需密码：true打开，平时false关闭
$isCloned = true;//设置是否已经Clone到本地,true:已经clone,直接pull,false:先clone.
//如果已经clone过,则直接拉去代码
if ($isCloned) {
    $requestBody = file_get_contents("php://input");
    if (empty($requestBody) && empty($is_test)) {
        die('send fail');
    }
    //解析码云发过来的JSON信息
    $content = json_decode($requestBody, true);
    //若是主分支且提交数大于0
    //密码要正确
    if($content['password'] == $password || !empty($is_test)){
        if($content['total_commits_count']>0 || !empty($is_test)) {
            if ($content['ref'] == "refs/heads/$branch" || !$branch || !empty($is_test)) {
                $cmd = "cd $savePath && sudo -u root $git reset --hard && sudo -u root $git clean -f && sudo -u root $git pull origin $branch 2>&1";
                exec($cmd, $result); //关键命令，拉取代码，2>&1后台执行
                $res_log = "[ PULL START ]" . PHP_EOL;
                if(!empty($is_test)){
                    $res_log .= date('Y-m-d H:i:s') . '执行测试！'. PHP_EOL;
                }else{
                    $res_log .= '[' . date('Y-m-d H:i:s') .'] ' . $content['user_name'] . ' 向 ' . $content['repository']['name'] . ' 项目的' . $content['ref'] . '分支push了' . $content['total_commits_count'] . '个commit：'. PHP_EOL;
                }

                $res_log .= $cmd. PHP_EOL;
                $res_log .= json_encode($result). PHP_EOL;
                $res_log .= "[ PULL END ]" . PHP_EOL;
                $res_log .= PHP_EOL . PHP_EOL;
                file_put_contents($logName.".log", $res_log, FILE_APPEND);//写入日志
                echo $result;
            } else {
                $res_log = "操作分支为：" . $content['ref'] . "未部署自动化脚本";
                file_put_contents($logName.".log", $res_log, FILE_APPEND);//写入日志
                echo $res_log;
            }
        }
    } else {
        file_put_contents($logName.".log", '密码错误!', FILE_APPEND);
        echo '密码错误!';
    }

}else {
    $res = "[ CLONE START ]".PHP_EOL;
    $res .= shell_exec("$git clone $gitSSHPath $savePath").PHP_EOL;
    $res .= "[ CLONE END ]".PHP_EOL;
    file_put_contents($logName.".log", $res, FILE_APPEND);//写入日志
}
